package net.fingersports.android.failinth;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.util.LruCache;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class PagerFragment extends SherlockFragment {
	private final static boolean LOG = Content.LOG;
	private final static String TAG = "PagerFragment";
	
	public final static String URL_RANDOM = "http://fail.in.th/?random&random_cat_id=1&random_cat_id=4";
	public final static String URL_FEED = "http://feeds.feedburner.com/failinth";
	public final static int MODE_RANDOM = 5000;
	public final static int MODE_LATEST = 5005;
	public final static String KEY_MODE = "key_mode";
	public final static String KEY_CONTENT_INDEX = "key_content_index";
	
	private final static int PIC_SIZE = 640;
	private final static int NEXT_CACHE = 5;
	private final static int PREV_CACHE = 15; // ~memory usage = (NEXT_CACHE + PREV_CACHE) * 1.5M
	private final static int LOAD_DELAY = 5;
	private static int sampleSize = 1;
	
	
	private final LinkedList<Content> mContents = new LinkedList<Content>();;
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;
	
	private LruCache<String, Bitmap> mMemCache;
	private File mCacheDir;
	
	private ScheduledExecutorService mDownloaderService;
	private boolean mTaskReady = true;
	private boolean mFirstTime = true;
	private int mLoadCount = 0;
	private int mLastViewPage = 0;
	private int mCurrentPage = 0;
	private int mPrevRemove = -1;
	private int mNextRemove = 65535;
	
	private MenuItem mIndicator;
	
	PagerFragmentEventsListener mEventsCallback = null;
	
	private SharedPreferences sharedPref = null;
	private int mMode = -1;
	
	private ShareActionProvider mShareProvider;
	
	public interface PagerFragmentEventsListener {
		public void onImageClick(View v);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// notify parent activity, this fragment have options menus
		setHasOptionsMenu(true);
		
		if (mMemCache == null) {
			// get memory class and assign used memory to LruCache
			final int memClass = ((ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
			final int cacheSize = 1024 * 1024 * memClass / 8;
			
			mMemCache = new LruCache<String, Bitmap>(cacheSize);
		}
		
		// get shared preferences and find mode
		sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mMode = sharedPref.getInt(KEY_MODE, MODE_RANDOM);
		
		// if screen small than image size (default 640) scale image down
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels;
		int height = metrics.heightPixels;
		int selected = 0;
		
		selected = width < height ? height : width;
		
		if (selected < PIC_SIZE) {
			sampleSize = PIC_SIZE / selected;
		}
		
		if (LOG) Log.i(
				TAG,
				String.format(
						Locale.US,
						"screen size: %dx%d, selected: %d, sample size: %d",
						width, height, selected, sampleSize));
		
		// setting up disk cache storage
		mCacheDir = getActivity().getExternalCacheDir();
		
		if (mCacheDir == null) {
			// fall back to internal storage
			mCacheDir = getActivity().getCacheDir();
		}
		
		if (LOG) Log.d(TAG, "cache dir: " + mCacheDir.toString());
		
		// start download FAIL content
		if (mMode == MODE_LATEST) {
			// download latest content url from feed and internal download that content
			LatestLoaderTask task = new LatestLoaderTask();
			task.execute(URL_FEED);
		}
		else {
			// load random content
			startLoadContent(URL_RANDOM);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// let fragment retain (save) its instance
		setRetainInstance(true);
		
		// prepare view pager by attach adapter and addition listener
		View root = inflater.inflate(R.layout.fragment_pager, container, false);
		mPager = (ViewPager) root.findViewById(R.id.pager);
		
		// (onCreate call before onCreateView)
		mPagerAdapter = new ContentAdapter(getActivity(), mContents, mMemCache);
		mPager.setPageMargin(20);
		mPager.setPageMarginDrawable(R.drawable.page_divider);
		mPager.setAdapter(mPagerAdapter);
		mPager.setOnPageChangeListener(mOnPageChangeListener);
		
		return root;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// make sure the callback interface was implemented
		try {
			mEventsCallback = (PagerFragmentEventsListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement PagerFragmentEventsListener");
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// if resume from instance start downloader service again and hide loading
		if (!mFirstTime) {
			startDownloaderService();
			setLoadingVisibility(View.GONE);
		}
		
		// if download finished, hide indicator
		if (mTaskReady) {
			displayIndicator(false);
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		// remove downloader service from system
		if (mDownloaderService != null) {
			mDownloaderService.shutdown();
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present
		// (ActionBarSherlock always make this true)
		inflater.inflate(R.menu.fragment_display, menu);
		
		// init share action provider
		MenuItem shareMenu = menu.findItem(R.id.menu_share);
		mShareProvider = (ShareActionProvider) shareMenu.getActionProvider();
		
		mIndicator = menu.findItem(R.id.menu_indicator);
		
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		
		// hide menu if content not ready
		if (mFirstTime) {
			menu.findItem(R.id.menu_web).setVisible(false);
			menu.findItem(R.id.menu_share).setVisible(false);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_web:
			onWebMenuClick(item);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Open browser and go to FAIL URL.
	 * 
	 * @param item Selected menu.
	 */
	private void onWebMenuClick(MenuItem item) {
		// open web page
		String url = mContents.get(mPager.getCurrentItem()).getUrl();
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(i);
	}
	
	public String getCurrentImageUrl() {
		return mContents.get(mCurrentPage).getImageUrl();
	}
	
	/**
	 * Create and set Intent to share provider.
	 */
	private void setShareIntent() {
		// create share intent and set to provider
		StringBuilder builder = new StringBuilder();
		// TEMPLATE: <TITLE> <URL> via @failinth
		builder.append(mContents.get(mPager.getCurrentItem()).getTitle());
		builder.append(" ");
		builder.append(mContents.get(mPager.getCurrentItem()).getUrl());
		builder.append(" via @failinth");
				
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_TEXT, builder.toString());
		
		mShareProvider.setShareIntent(i);
	}
	
	/**
	 * Display/Hide on screen loading indicator.
	 * 
	 * @param state Loading indicator state.
	 */
	private void setLoadingVisibility(int state) {
		// hide main loading indicator
		RelativeLayout loading = (RelativeLayout) getActivity().findViewById(R.id.pager_loading_container);
		loading.setVisibility(state);
	}
	
	/**
	 * Display/Hide ActionBar loading indicator.
	 * 
	 * @param state Loading indicator state.
	 */
	private void displayIndicator(boolean state) {
		if (mIndicator != null) {
			mIndicator.setVisible(state);
		}
	}
	
	/**
	 * Helper method for create and show Toast.
	 * 
	 * @param msg Toast message.
	 */
	private void makeToast(String msg) {
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * Helper method for create and show Toast.
	 * 
	 * @param resId Toast message id from Resource.
	 */
	private void makeToast(int resId) {
		makeToast(getString(resId));
	}

	/**
	 * Add bitmap to LruCache.
	 * 
	 * @param key Image URL.
	 * @param bmp Bitmap for cache.
	 */
	private void addBitmapToCache(String key, Bitmap bmp) {
		if (getBitmapFromCache(key) == null) {
			mMemCache.put(key, bmp);
		}
	}
	
	/**
	 * Retrieve bitmap from LruCache.
	 * 
	 * @param key Image URL.
	 * @return Bitmap from cache.
	 */
	public Bitmap getBitmapFromCache(String key) {
		return mMemCache.get(key);
	}
	
	/**
	 * Remove bitmap from LruCache.
	 * 
	 * @param key Image URL.
	 */
	private void removeBitmapFromCache(String key) {
		if (LOG) Log.i(TAG, "remove image: " + key);
		mMemCache.remove(key);
	}
	
	/**
	 * Remove bitmap in next cache and prepare (re-create) bitmap for old content in memory.
	 */
	private void moveBackward() {
		// reload removed image (back)
		if (mCurrentPage - PREV_CACHE >= 0) {
			final String url = mContents.get(mPrevRemove).getImageUrl();
			final ImageLoaderTask task = new ImageLoaderTask();
			task.execute(new String[] {url, "1"});
		}
		
		// remove image (next)
		final int remove = mCurrentPage + PREV_CACHE;
		if (remove < mLoadCount ) {
			mNextRemove = remove - 1;
			
			removeBitmapFromCache(mContents.get(mNextRemove).getImageUrl());
		}
	}
	
	/**
	 * Remove bitmap in previous cache and prepare (re-create) bitmap in next cache
	 * that destroy be moveBackword().
	 */
	private void moveForward() {
		// remove image (back)
		if (mCurrentPage > PREV_CACHE) {
			mPrevRemove = (mCurrentPage - PREV_CACHE) - 1;
			
			removeBitmapFromCache(mContents.get(mPrevRemove).getImageUrl());
		}
		
		// reload removed image (next)
		final int check = mCurrentPage + NEXT_CACHE;
		if (check > mNextRemove && check < mLoadCount) {
			final String url = mContents.get(mNextRemove).getImageUrl();
			final ImageLoaderTask task = new ImageLoaderTask();
			task.execute(new String[] {url, "2"});
		}
	}
	
	/**
	 * Prepare adapter, start scheduler service and hide indicator. 
	 */
	private void doAfterFirstDownload() {
		mFirstTime = false;
		startDownloaderService();
		mPagerAdapter.notifyDataSetChanged();
		
		// fix incorrect display share action menu
		setShareIntent();
		
		// if app still active, hide loading and update menu
		if (getActivity() != null) {
			setLoadingVisibility(View.GONE);
			getActivity().supportInvalidateOptionsMenu();
		}
	}
	
	/**
	 * Start download content from Internet, rely on mode (latest/random).
	 */
	private void startLoadContent() {
		if (LOG) Log.d(TAG, String.format(
				Locale.US,
				"cache:%d, load count:%d, current:%d",
				NEXT_CACHE, mLoadCount, mPager.getCurrentItem()));
		
		// limit download content, (may be) prevent OOM and save mobile data
		if (mLoadCount - mPager.getCurrentItem() > NEXT_CACHE) {
			if (LOG) Log.i(TAG, "Cache is full, stop download new content");
			return;
		}
		
		if (mMode == MODE_LATEST) {
			int size = mContents.size();
			startLoadContent(mContents.get(size - 1).getNextUrl());
		}
		else {
			startLoadContent(URL_RANDOM);
		}
	}
	
	/**
	 * Start download content from Internet.
	 * 
	 * @param url Content URL.
	 */
	private void startLoadContent(String url) {
		// start new task only if old task was completed
		if (mTaskReady) {
			mTaskReady = false;
			
			ContentLoaderTask task = new ContentLoaderTask();
			if (LOG) Log.i(TAG, "download content from: " + url);
			task.execute(url);
		}
	}
	
	/**
	 * Start scheduler service.
	 */
	private void startDownloaderService() {
		if (mDownloaderService != null) {
			return;
		}
		
		if (LOG) Log.d(TAG, "downloader service start");
		// start interval load new content
		mDownloaderService = Executors.newScheduledThreadPool(1);
		mDownloaderService.scheduleAtFixedRate(new Runnable() {
			public void run() {
				startLoadContent();
			}
		}, 3, LOAD_DELAY, TimeUnit.SECONDS);
	}
	
	/**
	 * Create Listener for detect ViewPager's state change.
	 */
	private final OnPageChangeListener mOnPageChangeListener = new OnPageChangeListener() {
		// use for change share intent correspond with current view pager's page
		
		@Override
		public void onPageScrollStateChanged(int arg0) {
			return;
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			return;
		}

		@Override
		public void onPageSelected(int arg0) {
			// arg0 = current selected page
			
			if (LOG) Log.i(TAG, "current page: " + arg0);
			
			if (LOG) Log.d(TAG, String.format(Locale.US, "data size: %d, prev: %d, next: %d", mContents.size(), mPrevRemove, mNextRemove));
			
			if (arg0 > mLastViewPage) {
				mLastViewPage = arg0;
			}
			
			if (arg0 > mCurrentPage) {
				mCurrentPage = arg0;
				moveForward();
			}
			else if (arg0 < mCurrentPage) {
				mCurrentPage = arg0;
				moveBackward();
			}
			
			setShareIntent();
		}
		
	};
	
	/**
	 * Helper class for download content from Internet.
	 * 
	 * @author Apichart Nakarungutti <mementototem@gmail.com>
	 *
	 */
	private class ContentLoaderTask extends AsyncTask<String, Void, Integer> {
		private final static String TAG = "ContentLoaderTask";
		
		private final static String PREFIX_FB = "http://www.facebook.com/plugins/comments.php?href=";
		private final static int OK = 1000;
		private final static int ERROR = 1001;
		
		private Content tempContent = new Content();
		
		@Override
		protected Integer doInBackground(String... args) {
			Elements selector = null;
			Iterator<Element> walker = null;
			Document doc = null;
			
			// display loading indicator on action bar
			final Activity activity = getActivity();
			if (activity != null) {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						displayIndicator(true);
					}
				});
			}
			
			try {
				
				// I want to display comment from Facebook before
				// old Disqus comment so extract them first
				doc = Jsoup.connect(PREFIX_FB + args[0]).timeout(Content.TIME_OUT).get();
				
				// but if unable to get, just ignore them
				if (doc != null) { 
					selector = doc.select("li.fbFeedbackPost");
					walker = selector.iterator();
					
					if (LOG) Log.i(TAG, "have " + selector.size() + " FB comments");
					
					while(walker.hasNext()) {
						Element item = walker.next();
						Comment result = Comment.extractFacebookComment(item);
						if (result == null) {
							break;
						}
						tempContent.addComment(result);
					}
					// clean walker out
					walker = null;
				}
				
				// OK, time to download main content
				doc = Jsoup.connect(args[0]).timeout(Content.TIME_OUT).get();
				
				// we care this, so if no data report with error
				if (doc == null) {
					if (LOG) Log.e(TAG, "document from " + args[0] + " is null");
					return ERROR;
				}
				
				// everything fine (may be), start extract data!
				
				// extract title from meta and remove web title
				selector = doc.select("meta[property=og:title]");
				String title = selector.attr("content");
				title = title.substring(0, title.length() - 13);
				tempContent.setTitle(new String(title));
				
				// extract description and separate it by new line
				selector = doc.select("meta[property=og:description]");
				String footer = selector.attr("content");
				
				// remove Facebook unwanted text from description
				int index = footer.indexOf("Facebook Comments comments");
				if (index > 0) {
					footer = footer.substring(0, index);
				}
				
				tempContent.setDescription(new String(footer));
				
				// extract image url
				selector = doc.select("meta[property=og:image]");
				tempContent.setImageUrl(selector.attr("content"));
				
				// extract FAIL url
				selector = doc.select("meta[property=og:url]");
				tempContent.setUrl(selector.attr("content"));
				
				// extract next (previous in web page) url
				selector = doc.select("link[rel=prev]");
				tempContent.setNextUrl(selector.attr("href"));
				
				// extract comments from web page (old comments from Disqus)
				selector = doc.select("ol.commentlist li.comment .comment-body");
				if (LOG) Log.i(TAG, "have " + selector.size() + " old comments");
				
				walker = selector.iterator();
				while(walker.hasNext()) {
					Element item = walker.next();
					Comment comment = Comment.extractComment(item);
					if(comment == null) {
						break;
					}
					tempContent.addComment(comment);
				}
			}
			catch (Exception e) {
				if (LOG) Log.e(TAG, e.toString());
				return ERROR;
			}
			finally {
				// clean out
				walker = null;
				selector = null;
				doc = null;
			}
			
			// main content is image, if no image make it error
			if (tempContent.getImageUrl() == null) {
				return ERROR;
			}
			
			// if image already in cache, use it
			Bitmap bmp = mMemCache.get(tempContent.getImageUrl());
			
			if (bmp != null) {
				 return OK;
			}
			
			// if image had been download, get it from disk
			bmp = Content.loadImageFromStorage(mCacheDir, tempContent.getImageUrl(), sampleSize);
			
			if (bmp == null) {
				// new, fresh image, download it from Internet and save it to disk cache
				bmp = Content.loadImageFromInternet(tempContent.getImageUrl(), sampleSize, mCacheDir);
			}
			
			if (bmp == null) {
				// can't get image from any source, tell loader we got error
				return ERROR;
			}
			
			// add image to memory cache
			addBitmapToCache(tempContent.getImageUrl(), bmp);
			return OK;
		}
		
		@Override
		protected void onPostExecute(Integer status) {
			if (status == ERROR) {
				if (LOG) Log.e(TAG, "unable to download content");
				makeToast(R.string.main_unable_download);
				
				if (mFirstTime) {
					getActivity().finish();
				}
			}
			else {
				if (LOG) Log.i(TAG, "download finished add to list");
				
				tempContent.setReadyState(true);
				
				mContents.addLast(tempContent);
				
				tempContent = null;
				
				mLoadCount++;
				if (LOG) Log.i(TAG, "download count: " + mLoadCount);
				
				if (mFirstTime) {
					doAfterFirstDownload();
				}
			}
			
			// ready to download new content
			mTaskReady = true;
			displayIndicator(false);
		}
	}
	
	/**
	 * <p>Helper class for download Image from disk storage.</p>
	 * 
	 * <p>If task can't find that image, try to download it from Internet.</p>
	 * 
	 * @author Apichart Nakarungutti <mementototem@gmail.com>
	 *
	 */
	private class ImageLoaderTask extends AsyncTask<String, Void, Integer> {
		private final static int OK = 1;
		private final static int ERROR = 0;
		
		public final static int MODE_BACK = 1;
		public final static int MODE_NEXT = 2;
		
		private int mMode = -1;
		
		@Override
		protected Integer doInBackground(String... args) {
			mMode = Integer.valueOf(args[1]);
			
			Bitmap bmp = Content.loadImageFromStorage(mCacheDir, args[0], sampleSize);
			
			if (bmp == null) {
				bmp = Content.loadImageFromInternet(args[0], sampleSize);
			}
			
			if (bmp != null) {
				addBitmapToCache(args[0], bmp);
				return OK;
			}
			
			return ERROR;
		}
		
		protected void onPostExecute(Integer status) {
			if (status == ERROR) {
				return;
			}
			
			if (mMode == MODE_BACK) {
				mPrevRemove--;
			}
			else if (mMode == MODE_NEXT) {
				mNextRemove++;
			}
		}
	}
	
	/**
	 * Helper class for retrieve latest content URL from Internet (via RSS).
	 * 
	 * @author Apichart Nakarungutti <mementototem@gmail.com>
	 *
	 */
	private class LatestLoaderTask extends AsyncTask<String, Void, String> {
		private final static String TAG = "LatestLoaderTask";
		@Override
		protected String doInBackground(String... args) {
			try {
				Document doc = Jsoup.connect(args[0]).get();
				if (doc == null) {
					return null;
				}
				
				Element latest = doc.select("item").get(0);
				Elements latestUrl = latest.getElementsByTag("feedburner:origLink");
				return latestUrl.text();
			}
			catch (Exception e) {
				if (LOG) Log.e(TAG, e.toString());
				return null;
			}
		}
		
		protected void onPostExecute(String url) {
			if (url == null) {
				makeToast(R.string.main_no_latest);
				if (LOG) Log.e(TAG, "unable to get latest url from feed");
				getActivity().finish();
				return;
			}
			
			if (LOG) Log.i(TAG, "latest url: " + url);
			// load latest content
			startLoadContent(url);
		}
	}
}
