package net.fingersports.android.failinth;

import java.io.File;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class PagerActivity extends SherlockFragmentActivity implements
										PagerFragment.PagerFragmentEventsListener {
	private static final boolean LOG = Content.LOG;
	private static final String TAG = "PagerActivity";
	
	public static final String EXTRA_IMAGE_URL = "extra_image_url";
	
	private static final int CODE_HOW_TO = 2400;
	private static final String KEY_HOW_TO = "key_how_to";
	private int howTo = -1;
	
	PagerFragment pagerFragment = null;
	
	SharedPreferences sharedPref = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_pager);
		
		if (pagerFragment == null) {
			pagerFragment = (PagerFragment) getSupportFragmentManager().findFragmentById(R.id.pager_fragment);
		}
		
		// enable http cache if have
		enableHttpResponseCache();
		
		// get shared preferences
		if (sharedPref == null) {
			sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		}
		
		// display how to
		howTo = sharedPref.getInt(KEY_HOW_TO, 0);
		if (howTo < CODE_HOW_TO) {
			displayHowTo();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.activity_main, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			onSettingMenuClick(item);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void onSettingMenuClick(MenuItem item) {
		Intent settingsIntent = new Intent(this, SettingsActivity.class);
		startActivity(settingsIntent);
	}
	
	private void enableHttpResponseCache() {
		try {
			long httpCacheSize = 10*1024*1024;
			File httpCacheDir = new File(getCacheDir(), "http");
			Class.forName("android.net.http.HttpResponseCache")
			.getMethod("install", File.class, long.class)
			.invoke(null, httpCacheDir, httpCacheSize);
		}
		catch (Exception httpResponseCacheNotAvailable) {
			if (LOG) Log.d(TAG, "HTTP response cache is unavailable");
		}
	}
	
	public void onImageClick(View v) {
		Intent zoomIntent = new Intent(this, ZoomActivity.class);
		zoomIntent.putExtra(EXTRA_IMAGE_URL, pagerFragment.getCurrentImageUrl());
		startActivity(zoomIntent);
	}
	
	/**
	 * Display how to use dialog, if have something new or in the first time.
	 */
	private void displayHowTo() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.main_howto_title);
		builder.setMessage(R.string.main_howto_message);
		builder.setNegativeButton(R.string.main_howto_ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
		
		TextView message = (TextView) dialog.findViewById(android.R.id.message);
		message.setGravity(Gravity.LEFT);
		
		
		// saved code into preference
		sharedPref.edit().putInt(KEY_HOW_TO, CODE_HOW_TO).commit();
	}
}
