package net.fingersports.android.failinth;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class ZoomActivity extends SherlockFragmentActivity {
	
	private ZoomFragment zoomFragment = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// load activity layout
		setContentView(R.layout.activity_zoom);
		
		// get attached fragment
		if (zoomFragment == null) {
			zoomFragment = (ZoomFragment) getSupportFragmentManager().findFragmentById(R.id.zoom_fragment);
		}
	}
}
