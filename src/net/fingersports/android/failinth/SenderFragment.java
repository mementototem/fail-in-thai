package net.fingersports.android.failinth;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;

public class SenderFragment extends SherlockFragment {
	@SuppressWarnings("unused")
	private final static boolean LOG = Content.LOG;
	private final static String TAG = "SenderFragment";
	
	private final static int IMAGE_SIZE = 128;
	
	private Bitmap mSelectedImage;
	private Uri mImageUri = null;
	private boolean mFirstTime = true;
	
	SenderFragmentEventsListener mEventsCallback;
	
	public interface SenderFragmentEventsListener {
		public void onSendClick(View v);
		public void onSelectPhotoClick(View v);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		// let's fragment retain (save) its instance
		setRetainInstance(true);
		
		// inflate layout to fragment
		return inflater.inflate(R.layout.fragment_sender, container, false);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// this will not call when resume (retain instance ignore it)
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// make sure the callback interface was implemented
		try {
			mEventsCallback = (SenderFragmentEventsListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement SenderFragmentEventsListener");
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// load selected image from retain instance
		setSelectedImage();
		
		if (mFirstTime == false) {
			hideChooseImageButton();
		}
	}
	
	/**
	 * Set user name in By: input box
	 * 
	 * @param name user name
	 */
	public void setUserName(String name) {
		EditText byView = (EditText) getActivity().findViewById(R.id.sender_input_by);
		byView.setText(name);
	}
	
	/**
	 * Decode image from URI and display it.
	 * 
	 * @param uri Selected image URI
	 */
	public void setImage(Uri uri) {
		mImageUri = uri;
		
		try {
			// fake decode image (for calculate size)
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri), null, options);
			
			// calculate image output size
			int oWidth = options.outWidth;
			int oHeight = options.outHeight;
			int scale = 1;
			
			while (true) {
				if (oWidth / 2 < IMAGE_SIZE || oHeight / 2 < IMAGE_SIZE) {
					break;
				}
				
				oWidth = oWidth / 2;
				oHeight = oHeight / 2;
				scale = scale * 2;
			}
			
			options.inJustDecodeBounds = false;
			options.inSampleSize = scale;
			
			// real decode image
			mSelectedImage = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri), null, options);
			
			setSelectedImage();
			
			if (mFirstTime == true) {
				hideChooseImageButton();
				mFirstTime = false;
			}
		}
		catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}
	
	/**
	 * Get selected image URI.
	 * 
	 * @return Selected image URI
	 */
	public Uri getImageUri() {
		return mImageUri;
	}
	
	/**
	 * Helper method for display image.
	 */
	private void setSelectedImage() {
		if (mSelectedImage == null) {
			return;
		}
		
		ImageView imageView = (ImageView) getActivity().findViewById(R.id.sender_image);
		if (imageView != null) {
			imageView.setImageBitmap(mSelectedImage);
		}
	}
	
	private void hideChooseImageButton() {
		Button chooseButton = (Button) getActivity().findViewById(R.id.sender_select_image);
		chooseButton.setVisibility(View.GONE);
		
		ImageView chooseView = (ImageView) getActivity().findViewById(R.id.sender_image);
		chooseView.setVisibility(View.VISIBLE);
	}
}
