package net.fingersports.android.failinth;

import java.util.Iterator;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;

/**
 * <p>A helper class for store user comment in web page, include name and time.</p>
 * 
 * <p>This function have static method for parse comment from Facebook Comment
 * and WordPress comment (this method limit by theme) by use
 * <a href="http://jsoup.org">Jsoup</a>'s Element class as source.</p>
 * 
 * @author Apichart Nakarungutti <mementototem@gmail.com>
 */
public class Comment {
	private final static boolean LOG = Content.LOG;
	private final static String TAG = "Comment";
	
	private final static String USER = "unknown";
	private final static String TIME = "long long ago";
	private final static String MSG = "&#8230;";
	
	private String mUser;
	private String mTime;
	private String mMessage;
	
	Comment() {
		mUser = USER;
		mTime = TIME;
		mMessage = MSG;
	}
	
	Comment(String user, String time, String message) {
		this.mUser = user;
		this.mTime = time;
		this.mMessage = message;
	}
	
	Comment(Comment another) {
		this.mUser = another.getUser();
		this.mTime = another.getTime();
		this.mMessage = another.getMessage();
	}
	
	/**
	 * Set commentator's name for this comment.
	 * 
	 * @param name Commentator's name.
	 */
	public void setUser(String name) {
		mUser = name;
	}
	
	/**
	 * Get commentator's name.
	 * 
	 * @return Commentator's name.
	 */
	public String getUser() {
		return mUser;
	}
	
	/**
	 * Set comment's post time.
	 * 
	 * @param time Time when user post comment.
	 */
	public void setTime(String time) {
		this.mTime = time;
	}
	
	/**
	 * Get comment's post time.
	 * 
	 * @return Time wehn user post comment.
	 */
	public String getTime() {
		return mTime;
	}
	
	/**
	 * Set comment's message.
	 * 
	 * @param msg Comment's message.
	 */
	public void setMessage(String msg) {
		mMessage = msg;
	}
	
	/**
	 * Get comment's message.
	 * 
	 * @return Comment's message.
	 */
	public String getMessage() {
		return mMessage;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(mUser);
		builder.append(" said ");
		builder.append(mMessage.substring(0, 10));
		builder.append("&#8230;");
		
		return builder.toString();
	}
	
	/**
	 * <p>Parse Facebook Comment by use <a href="http://jsoup.org">Jsoup</a>'s Element class
	 * as source.</p>
	 * 
	 * <p>This static method also fix wrong (double) date in post time, but still
	 * can't remove wrong space in word (that a Facebook issue).<p>
	 * 
	 * @param item Specified comment from Jsoup#selector method.
	 * @return Parsed comment.
	 */
	public static Comment extractFacebookComment(Element item) {
		Comment comment = new Comment();
		
		try {
			// extract commenter name
			Elements selector = item.select("div.postContainer a.profileName");
			comment.setUser(selector.first().text());
			
			// extract time
			selector = item.select("div.postContainer div.postContent abbr");
			// fixed double 'วัน วัน' in time (Thai locale)
			String time = selector.first().text();
			time = time.replace("วัน วัน", "วัน");
			comment.setTime(time);
			
			// extract comment message
			selector = item.select("div.postContainer div.postContent div.postText");
			String msg = selector.first().html();
			StringBuilder builder = new StringBuilder();
			builder.append(android.text.Html.fromHtml(msg).toString());
			
			// remove last new line from comment's message
			if (builder.lastIndexOf("\n") == builder.length() - 1) {
				builder.setLength(builder.length() - 1);
			}
			
			comment.setMessage(builder.toString());
			
			return comment;
		}
		catch (Exception e) {
			if (LOG) Log.e(TAG, e.toString());
			return null;
		}
	}
	
	/**
	 * <p>Parse WordPress comment by use <a href="http://jsoup.org">Jsoup</a>'s Element class
	 * as source.</p>
	 * 
	 * <p>The limitation of this method is it belong to WordPress theme,
	 * if WordPress theme developer change the way to display comment,
	 * this method might parse comment incorrectly.</p>
	 * 
	 * @param item Specified comment from Jsoup#selector method.
	 * @return Parsed comment.
	 */
	public static Comment extractComment(Element item) {
		Comment comment = new Comment();
		
		try {
			// extract commenter name
			Elements selector = item.select(".comment-author .fn");
			comment.setUser(selector.first().text());
			
			// extract time
			selector = item.getElementsByClass("comment-meta");
			String time = selector.first().text();
			comment.setTime(time);
			
			
			// extract comment message
			selector = item.getElementsByTag("p");
			
			Iterator<Element> walker = selector.iterator();
			StringBuilder builder = new StringBuilder();
			
			while(walker.hasNext()) {
				builder.append(walker.next().text());
				builder.append("\n");
			}
			
			// remove last new line from comment's message
			builder.setLength(builder.length() - 1);
			
			comment.setMessage(builder.toString());
			
			return comment;
		}
		catch (Exception e) {
			if (LOG) Log.e(TAG, e.toString());
			return null;
		}
	}
}
