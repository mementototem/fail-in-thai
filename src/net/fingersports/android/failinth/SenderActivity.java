package net.fingersports.android.failinth;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SenderActivity extends SherlockFragmentActivity implements
										SenderFragment.SenderFragmentEventsListener {
	
	private final static boolean LOG = Content.LOG;
	private final static String TAG = "SenderActivity";
	
	private final static int REQUEST_CAMERA = 300;
	private final static int REQUEST_FILE = 301;
	private final static int REQUEST_MAIL = 302;
	
	public final static String KEY_USER_NAME = "key_user_name";
	SharedPreferences sharedPref = null;
	
	private Uri mImageUri = null;
	private Uri mMailAddress = Uri.parse("mailto:fail.in.th@gmail.com");
	
	SenderFragment senderFragment = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// load activity layout
		setContentView(R.layout.activity_sender);
		
		// get attached fragment
		if (senderFragment == null) {
			senderFragment = (SenderFragment) getSupportFragmentManager().findFragmentById(R.id.sender_fragment);
		}
		
		// get shared preferences
		if (sharedPref == null) {
			sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		String name = sharedPref.getString(KEY_USER_NAME, "");
		
		senderFragment.setUserName(name);
	}
	
	/**
	 * Create a menu for user to select source of FAIL.
	 * 
	 * @param v View that call this method.
	 */
	@Override
	public void onSelectPhotoClick(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.sender_select_source);
		builder.setItems(R.array.sender_source, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					getImageFromFile();
					break;
				case 1:
					getImageFromCamera();
					break;
				}
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	@Override
	public void onSendClick(View v) {
		EditText titleView = (EditText) findViewById(R.id.sender_input_title);
		EditText fromView = (EditText) findViewById(R.id.sender_input_from);
		EditText byView = (EditText) findViewById(R.id.sender_input_by);
		
		String title = titleView.getText().toString();
		String from = fromView.getText().toString();
		String by = byView.getText().toString();
		
		if (mImageUri == null) {
			mImageUri = senderFragment.getImageUri();
		}
		
		// check form input
		if (title == null || from == null || by == null || mImageUri == null) {
			Toast.makeText(this, getString(R.string.sender_must_fill), Toast.LENGTH_SHORT).show();
			return;
		}
		
		// build body message
		StringBuilder builder = new StringBuilder();
		builder.append(getString(R.string.sender_pre_intro));
		builder.append(getString(R.string.sender_pre_title));
		builder.append(title);
		builder.append("\n\n");
		builder.append(getString(R.string.sender_pre_by));
		builder.append(by);
		builder.append("\n\n");
		builder.append(getString(R.string.sender_pre_from));
		builder.append(from);
		builder.append("\n\n");
		builder.append(getString(R.string.sender_pre_sign));
		
		// create/prepare new mail intent and call it
		Intent mail = new Intent(Intent.ACTION_SENDTO);
		mail.setType("text/plain");
		mail.setData(mMailAddress);
		mail.putExtra(Intent.EXTRA_SUBJECT, title + " - " + by);
		mail.putExtra(Intent.EXTRA_TEXT, builder.toString());
		mail.putExtra(Intent.EXTRA_STREAM, mImageUri);
		
		startActivityForResult(Intent.createChooser(mail, getString(R.string.sender_intent)), REQUEST_MAIL);
		
		// save user info for later use
		sharedPref.edit().putString(KEY_USER_NAME, by).commit();
		
		finish();
	}
	
	/**
	 * Create intent for retrieve image from camera app.
	 */
	private void getImageFromCamera() {
		final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HHmmss", Locale.US);
		final Date date = new Date();
		final String filename = format.format(date) + ".jpg";
		
		final File outputFile = new File(getExternalFilesDir(null), filename);
		mImageUri = Uri.fromFile(outputFile);
		
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
		startActivityForResult(cameraIntent, REQUEST_CAMERA);
	}
	
	/**
	 * Create intent for retrieve image from file.
	 */
	private void getImageFromFile() {
		final Intent fileIntent = new Intent(Intent.ACTION_PICK);
		fileIntent.setType("image/*");
		startActivityForResult(fileIntent, REQUEST_FILE);
	}
	
	/**
	 * Retrieve data (image) from called activity result.
	 * 
	 * @param requestCode Code correspond with the intent activity
	 * @param resultCode Return status
	 * @param data Return data
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (resultCode == Activity.RESULT_CANCELED) {
			if (LOG) Log.d(TAG, "user cancelled");
			return;
		}
		
		switch (requestCode) {
		case REQUEST_MAIL:
			finish();
			break;
		case REQUEST_CAMERA:
			if (LOG) Log.d(TAG, "get image from camera");
			
			getContentResolver().notifyChange(mImageUri, null);
			
			break;
		case REQUEST_FILE:
			if (LOG) Log.d(TAG, "get image from file");
			
			mImageUri = data.getData();
			
			break;
		}
		
		if (LOG) Log.i(TAG, "selected file: " + mImageUri.toString());
		
		senderFragment.setImage(mImageUri);
		
		super.onActivityResult(requestCode, resultCode, data);
	}
}
