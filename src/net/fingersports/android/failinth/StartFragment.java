package net.fingersports.android.failinth;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Random;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.actionbarsherlock.app.SherlockFragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class StartFragment extends SherlockFragment {
	private final static boolean LOG = Content.LOG;
	
	public final static String URL_MAIN = "http://fail.in.th";
	StartFragmentEventsListener mEventsCallback;
	
	private String mAdvUrl = null;
	private Bitmap mAdvImage = null;
	private File mCacheDir = null;
	
	// *must have* interface for deliver messages to StartActivity
	public interface StartFragmentEventsListener {
		public void onStartFromLatestClick(View v);
		public void onStartFromRandomClick(View v);
		public void onAdvClick(View v);
		public void onTryClick(View v);
		public void onSendFailClick(View v);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		// let fragment retain (save) its instance
		setRetainInstance(true);
		
		// inflate layout to fragment
		return inflater.inflate(R.layout.fragment_start, container, false);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// this will not call when resume (retain instance ignore it)
		
		// setting up disk cache storage
		mCacheDir = getActivity().getExternalCacheDir();
		
		if (mCacheDir == null) {
			// fall back to internal storage
			mCacheDir = getActivity().getCacheDir();
		}
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// make sure the callback interface was implemented
		try {
			mEventsCallback = (StartFragmentEventsListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement StartFragmentEventsListener");
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// load advertise image from retain instance
		setAdvImage();
		
		checkConnection();
	}
	
	/**
	 * Check network connection, and do stuff rely on connection state.
	 */
	public void checkConnection() {
		// get connection state
		final ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		
		if (activeNetwork != null && activeNetwork.isConnected()) {
			haveInternet();
		}
		else {
			noInternet();
		}
	}
	
	/**
	 * Download advertise and hide no connection View.
	 */
	private void haveInternet() {
		if (mAdvImage == null) {
			ImageView advArea = (ImageView) getActivity().findViewById(R.id.start_adv);
			AdvLoaderTask task = new AdvLoaderTask(advArea);
			task.execute(URL_MAIN);
		}
		
		setVisibility(View.GONE, View.VISIBLE);
	}
	
	/**
	 * Display no connection View.
	 */
	private void noInternet() {
		setVisibility(View.VISIBLE, View.INVISIBLE);
	}
	
	/**
	 * Display/Hide View on Fragment
	 * 
	 * @param nState No connection Button/View state.
	 * @param bState Main Button/View state.
	 */
	private void setVisibility(int nState, int bState)  {
		// notice views
		Button tryButton = (Button) getActivity().findViewById(R.id.start_try);
		TextView netText = (TextView) getActivity().findViewById(R.id.start_net);
		tryButton.setVisibility(nState);
		netText.setVisibility(nState);
		
		// latest/random buttons
		Button latest = (Button) getActivity().findViewById(R.id.start_latest);
		Button random = (Button) getActivity().findViewById(R.id.start_random);
		ImageView adv = (ImageView) getActivity().findViewById(R.id.start_adv);
		latest.setVisibility(bState);
		random.setVisibility(bState);
		adv.setVisibility(bState);
	}
	
	/**
	 * Display advertise image.
	 */
	private void setAdvImage() {
		if (mAdvImage == null) {
			return;
		}
		
		ImageView advArea = (ImageView) getActivity().findViewById(R.id.start_adv);
		if (advArea != null) {
			advArea.setImageBitmap(mAdvImage);
		}
	}
	
	/**
	 * Get advertise URL.
	 * 
	 * @return Advertise URL.
	 */
	public String getAdvUrl() {
		return mAdvUrl;
	}
	
	/**
	 * Helper class for retrieve advertise data (URL/image) from web page.
	 * 
	 * @author Apichart Nakarungutti
	 *
	 */
	private class AdvLoaderTask extends AsyncTask<String, Void, Integer> {
		private final static String TAG = "AdvLoaderTask";
		private final static int OK = 1000;
		private final static int ERROR = 1001;
		
		private String imgUrl = null;
		private String linkUrl = null;
		private final WeakReference<ImageView> advAreaRef;
		
		public AdvLoaderTask(ImageView iv) {
			advAreaRef = new WeakReference<ImageView>(iv);
		}
		
		@Override
		protected Integer doInBackground(String... args) {
			// extract advertise from web page
			try {
				Document doc = Jsoup.connect(args[0]).get();
				
				if (doc == null) {
					return ERROR;
				}
				
				Elements advWrapper = doc.select("div#wp125adwrap_1c div.wp125ad");
				
				// random advertise id and selected that advertise index
				Random randomGen = new Random();
				int advIndex = randomGen.nextInt(advWrapper.size());
				
				Element selectedAdv = advWrapper.get(advIndex);
				Elements advA = selectedAdv.getElementsByTag("a");
				Elements advImg = selectedAdv.getElementsByTag("img");
				
				linkUrl = advA.attr("href");
				imgUrl = advImg.attr("src");
				
				// if not start with http(s):// add URL_MAIN as prefix
				if (!(linkUrl.startsWith("http://") || linkUrl.startsWith("https://"))) {
					linkUrl = URL_MAIN + "/" + linkUrl;
				}
				
				if (LOG) Log.i(TAG, String.format(Locale.US, "have %d banners, select #%d", advWrapper.size(), advIndex));
			}
			catch (Exception e) {
				if (LOG) Log.e(TAG, e.toString());
			}
			
			if (imgUrl == null) {
				return ERROR;
			}
			
			// download advertise image
			mAdvImage = Content.loadImageFromStorage(mCacheDir, imgUrl);
			
			if (mAdvImage == null) {
				mAdvImage = Content.loadImageFromInternet(imgUrl, mCacheDir);
			}
			
			if (mAdvImage == null) {
				return ERROR;
			}
			
			mAdvUrl = linkUrl;
			return OK;
		}
		
		@Override
		protected void onPostExecute(Integer status) {
			if (status == ERROR || advAreaRef == null) {
				Toast.makeText(getActivity(), getString(R.string.start_no_adv), Toast.LENGTH_SHORT).show();
				return;
			}
			
			if (LOG) Log.i(TAG, String.format(Locale.US, "adv url: %s", mAdvUrl));
			
			final ImageView iv = advAreaRef.get();
			if (iv != null) {
				iv.setImageBitmap(mAdvImage);
			}
		}
	}
}
