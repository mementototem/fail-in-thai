package net.fingersports.android.failinth;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * <p>Helper class for store content (FAIL) data.</p>
 * 
 * <p>This class doesn't store Bitmap data, you must implement your own Bitmap storage
 * like LruCache and use ImageUrl as reference.</p>
 * 
 * @author Apichart Nakarungutti
 */
public class Content {
	/**
	 * Use for enable/disable log system (Log Cat) by check this variable first
	 */
	public final static boolean LOG = false;
	
	public final static String TAG = "Content";
	public final static String URL_MAIN = "http://fail.in.th";
	public final static int TIME_OUT = 5000;
	
	private boolean mReady;
	private String mTitle;
	private String mDesc;
	private String mImageUrl;
	private String mUrl;
	private String mNextUrl;
	private ArrayList<Comment> mComments;
	
	public Content() {
		mReady = false;
		mComments = new ArrayList<Comment>();
	}
	
	public Content(Content another) {
		this.mReady = another.isReady();
		this.mTitle = another.getTitle();
		this.mDesc = another.getDescription();
		this.mImageUrl = another.getImageUrl();
		this.mUrl = another.getUrl();
		this.mNextUrl = another.getNextUrl();
		this.mComments = another.getComments();
	}
	
	/**
	 * Set title of content.
	 * 
	 * @param text Title.
	 */
	public void setTitle(String text) {
		mTitle = text;
	}
	
	/**
	 * Get content's title.
	 * 
	 * @return Content's title.
	 */
	public String getTitle() {
		return mTitle;
	}
	
	/**
	 * Set content's description (like by {username}).
	 * 
	 * @param text Description
	 */
	public void setDescription(String text) {
		mDesc = text;
	}
	
	/**
	 * Get content's description.
	 * 
	 * @return Content's description
	 */
	public String getDescription() {
		return mDesc;
	}
	
	/**
	 * Set content's image URL.
	 * 
	 * @param imgUrl URL of content's image.
	 */
	public void setImageUrl(String imgUrl) {
		mImageUrl = imgUrl;
	}
	
	/**
	 * Get content's image URL.
	 * 
	 * @return URL of content's image.
	 */
	public String getImageUrl() {
		return mImageUrl;
	}
	
	/**
	 * Set content's URL.
	 * 
	 * @param url URL of content itself.
	 */
	public void setUrl(String url) {
		this.mUrl = url;
	}
	
	/**
	 * Get content's URL.
	 * 
	 * @return URL of content.
	 */
	public String getUrl() {
		return mUrl;
	}
	
	/**
	 * Set URL of content next to this content (previous link in web page).
	 * 
	 * @param url URL of next content.
	 */
	public void setNextUrl(String url) {
		mNextUrl = url;
	}
	
	/**
	 * Get URL of next content (previous link in web page).
	 * 
	 * @return URL of next content.
	 */
	public String getNextUrl() {
		return mNextUrl;
	}
	
	/**
	 * Set content state.
	 * 
	 * @param state Content state.
	 */
	public void setReadyState(boolean state) {
		mReady = state;
	}
	
	/**
	 * Indicate content is ready for use or not.
	 * 
	 * @return TRUE if content is ready for use.
	 */
	public boolean isReady() {
		return mReady;
	}
	
	/**
	 * Add single comment into content's comment list.
	 * 
	 * @param comment Comment.
	 */
	public void addComment(Comment comment) {
		mComments.add(comment);
	}
	
	/**
	 * Get collection of comments about this content.
	 * 
	 * @return Collection of comments.
	 */
	public ArrayList<Comment> getComments() {
		return mComments;
	}
	
	/**
	 * Count number of comments about this content.
	 * 
	 * @return Amount of comments
	 */
	public int countComments() {
		return mComments.size();
	}
	
	/**
	 * Download image from Internet and convert to Bitmap data.
	 *  
	 * @param strUrl URL of image.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromInternet(String strUrl) {
		return Content.loadImageFromInternet(strUrl, 1, null);
	}
	
	/**
	 * Download image from Internet, convert to Bitmap data and store in cache folder.
	 * 
	 * @param strUrl URL of image.
	 * @param dir Cache folder.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromInternet(String strUrl, File dir) {
		return loadImageFromInternet(strUrl, 1, dir);
	}
	
	/**
	 * Download image from Internet, convert to Bitmap data also you can specified quality of image.
	 *  
	 * @param strUrl URL of image.
	 * @param sampleSize Quality of image, (1 = same as source) see BitmapFactory class.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromInternet(String strUrl, int sampleSize) {
		return loadImageFromInternet(strUrl, sampleSize, null);
	}
	
	/**
	 * Download image from Internet, convert to Bitmap data and store in cache folder
	 * also you can specified quality of image.
	 * 
	 * @param strUrl URL of image.
	 * @param sampleSize Quality of image, (1 = same as source) see BitmapFactory class.
	 * @param dir Cache folder.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromInternet(String strUrl, int sampleSize, File dir) {
		if (strUrl == null || sampleSize <= 0) {
			return null;
		}
		
		if (!(strUrl.startsWith("http://") || strUrl.startsWith("https://"))) {
			strUrl = URL_MAIN + strUrl;
		}
		
		HttpURLConnection http = null;
		HttpsURLConnection https = null;
		
		Bitmap bmp = null;
		
		if (LOG) Log.i(TAG, "download image from " + strUrl);
		
		// try to connect to server
		try {
			// download image from Internet
			URL url = new URL(strUrl);
			InputStream inStream = null;
			
			if (strUrl.startsWith("http://")) {
				http = (HttpURLConnection) url.openConnection();
				http.setUseCaches(true);
				http.setConnectTimeout(TIME_OUT);
				inStream = new BufferedInputStream(http.getInputStream());
			}
			else if (strUrl.startsWith("https://")) {
				https = (HttpsURLConnection) url.openConnection();
				https.setUseCaches(true);
				https.setConnectTimeout(TIME_OUT);
				inStream = new BufferedInputStream(https.getInputStream());
			}
			else {
				return null;
			}
			
			// prepare file saving
			FileOutputStream fileStream = null;
			
			if (dir != null) {
				final File savedFile = new File(dir.getAbsolutePath() + "/" + url2filename(strUrl));
				fileStream = new FileOutputStream(savedFile);
				
				if (LOG) Log.i(TAG, "save image to " + savedFile.toString());
			}
			
			// prepare bitmap decoding
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			options.inSampleSize = sampleSize;
			
			// read from input stream save to file and store for decode
			final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			int nRead = 0;
			byte[] data = new byte[16384];
			
			while ((nRead = inStream.read(data)) != -1) {
				byteStream.write(data, 0, nRead);
				
				if (fileStream != null) {
					fileStream.write(data, 0, nRead);
				}
			}
			
			// close stream
			inStream.close();
			
			
			if (fileStream != null) {
				fileStream.flush();
				fileStream.close();
			}
			
			// prepare byte stream for decode
			byteStream.flush();
			
			final byte[] imgByte = byteStream.toByteArray();
			
			byteStream.close();
			
			bmp = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length, options);
		}
		catch (MalformedURLException e) {
			if (LOG) Log.e(TAG, "MalformedURL: " + e.toString());
		}
		catch (IOException e) {
			if (LOG) Log.e(TAG, "IO Error: " + e.toString());
		}
		catch (Exception e) {
			if (LOG) Log.e(TAG, "Unknown Error: " + e.toString());
		}
		finally {
			if (http != null) {
				http.disconnect();
			}
			if (https != null) {
				https.disconnect();
			}
		}
		
		return bmp;
	}
	
	/**
	 * Load image from cache folder instead of Internet.
	 * 
	 * @param dir Cache folder.
	 * @param url URL of image.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromStorage(File dir, String url) {
		return loadImageFromStorage(dir, url, 1);
	}
	
	/**
	 * Load image from cache folder instead of Internet and specified quality of image.
	 * 
	 * @param dir Cache folder.
	 * @param url URL of image.
	 * @param sampleSize Quality of image, (1 = same as source) see BitmapFactory class.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromStorage(File dir, String url, int sampleSize) {
		final String imgFile = dir.getAbsolutePath() + "/" + url2filename(url);
		
		// prepare bitmap decoding
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inSampleSize = sampleSize;
		
		return BitmapFactory.decodeFile(imgFile, options);
	}
	
	/**
	 * Convert URL string into acceptable filename.
	 * 
	 * @param url URL string.
	 * @return Usable filename.
	 */
	private static String url2filename(String url) {
		if (url.startsWith("http://")) {
			url = url.substring(7);
		}
		else if (url.startsWith("https://")) {
			url = url.substring(8);
		}
		
		return new String(url.replace("/", "-"));
	}
}
