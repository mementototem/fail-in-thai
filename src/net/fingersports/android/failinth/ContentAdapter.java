package net.fingersports.android.failinth;

import java.util.LinkedList;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Custom append View into ViewPager instead of use Fragment.
 * 
 * @author Apichart Nakarungutti <mementototem@gmail.com>
 *
 */
public class ContentAdapter extends PagerAdapter {
	private static final int NONE = -1;
	
	private final Context mContext;
	private final LayoutInflater mInflater;
	private final LinkedList<Content> mData;
	private final LruCache<String, Bitmap> mCache;
	
	private int mPrimary = NONE;
	private int mNewPrimary = NONE;
	
	public ContentAdapter(Context context, LinkedList<Content> data, LruCache<String, Bitmap> cache) {
		mContext = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mData = data;
		mCache = cache;
	}
	
	/**
	 * <p>Create View by inflate layout from resource,
	 * this method also check for tablet and display in suitable layout.</p>
	 * 
	 * <p>This method call by ViewPager, you must not call this by yourself.</p>
	 * 
	 *  @param container Page in ViewPager.
	 *  @param position Index of item in ArrayList.
	 *  
	 *  @return View that ready for display.
	 */
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		final View root = mInflater.inflate(R.layout.view_display, container, false);
		
		ListView displayList = (ListView) root.findViewById(R.id.display_list);
		ImageView imageView = (ImageView) root.findViewById(R.id.display_image);
		
		final View header;
		
		if (imageView != null) {
			header = mInflater.inflate(R.layout.part_meta, null);
		}
		else {
			header = mInflater.inflate(R.layout.list_content, null);
			imageView = (ImageView) header.findViewById(R.id.display_image);
		}
		
		setContentImage(imageView, position);
		setHeaderMeta(header, position);
		
		displayList.addHeaderView(header, null, false);
		
		CommentAdapter commentAdapter = new CommentAdapter(mContext, R.layout.list_comment, mData.get(position).getComments());
		displayList.setAdapter(commentAdapter);
		
		final PageInfo info = new PageInfo();
		info.position = position;
		info.imageView = imageView;
		
		root.setTag(info);
		
		container.addView(root);
		return root;
	}

	/**
	 * Count number of page (data size).
	 * 
	 * @return Amount of total page.
	 */
	@Override
	public int getCount() {
		return mData.size();
	}
	
	/**
	 * <p>Remove View from memory.</p>
	 * 
	 * <p>This method call by ViewPager, you must not call this by yourself.</p>
	 * 
	 * @param container Page in ViewPager.
	 * @param position Index of data in ArrayList.
	 * @param obj View inside Page.
	 */
	@Override
	public void destroyItem(ViewGroup container, int position, Object obj) {
		final View v = (View) obj;
		container.removeView(v);
	}

	/**
	 * <p>Compare between View and Object of View</p>
	 * 
	 * <p>This method call by ViewPager, you must not call this by yourself.</p>
	 * 
	 * @param arg0 View.
	 * @param arg1 Object of View.
	 * 
	 * @return TRUE, if there are same object.
	 */
	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}
	
	/**
	 * <p>Set primary View (View that display on screen).</p>
	 * 
	 * <p>This method call by ViewPager, you must not call this by yourself.</p>
	 * 
	 *  @param container Page of ViewPager.
	 *  @param position Index of data in ArrayList.
	 *  @param object Object (View) of primary View.
	 */
	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		super.setPrimaryItem(container, position, object);
		mNewPrimary = position;
	}
	
	/**
	 * <p>Call when ViewPager finish update Page.</p>
	 * 
	 * <p>I use this method to update content image, if user want to view content
	 * that image already gone, I re-attach it back by this.</p>
	 * 
	 * <p>It would be use AysncTask if you want to download image only when user come back
	 * But I already prepare the image, so just add it back.</p>
	 * 
	 * <p>This method call by ViewPager, you must not call this by yourself.</p>
	 */
	@Override
	public void finishUpdate(ViewGroup container) {
		if (mNewPrimary != NONE) {
			if (mNewPrimary != mPrimary) {
				final int count = container.getChildCount();
				// in default off screen limit, pager has 3 children;
				
				for (int i = 0; i < count; i++) {
					final View view = container.getChildAt(i);
					final PageInfo info = (PageInfo) view.getTag();
					final int position = info.position;
					if (position == mNewPrimary) {
						// update image
						setContentImage(info.imageView, position);
						break;
					}
				}
				
				mPrimary = mNewPrimary;
			}
			
			mNewPrimary = NONE;
		}
	}
	
	/**
	 * Attach bitmap image into ImageView.
	 * 
	 * @param iv ImageView for display image.
	 * @param position Index of data in ArrayList.
	 */
	private void setContentImage(ImageView iv, int position) {
		if (iv.getDrawable() == null) {
			final String imgUrl = mData.get(position).getImageUrl();
			iv.setImageBitmap(getBitmapFromCache(imgUrl));
		}
	}
	
	/**
	 * Set title, description and comments count from content.
	 * 
	 * @param root Container of View.
	 * @param position Index of data in ArrayList.
	 */
	private void setHeaderMeta(View root, int position) {
		TextView titleView = (TextView) root.findViewById(R.id.meta_title);
		String title = mData.get(position).getTitle();
		titleView.setText(title);
		
		TextView descView = (TextView) root.findViewById(R.id.meta_desc);
		String desc = mData.get(position).getDescription();
		descView.setText(desc);
		
		final int commentCount = mData.get(position).countComments();
		
		if (commentCount <= 0) {
			return;
		}
		
		String comments;
		
		if (commentCount == 1) {
			comments = mContext.getString(R.string.meta_comment_one);
		}
		else {
			String template = mContext.getString(R.string.meta_comment_count);
			comments = String.format(Locale.getDefault(), template, commentCount);
		}
		
		TextView commentView = (TextView) root.findViewById(R.id.meta_comments);
		commentView.setText(comments);
	}
	
	/**
	 * Retrieve image's bitmap from LruCache.
	 * 
	 * @param key Image URL.
	 * @return Image bitmap.
	 */
	private Bitmap getBitmapFromCache(String key) {
		return mCache.get(key);
	}
	
	/**
	 * Helper class for store data index and ImageView reference.
	 * 
	 * @author Apichart Nakarungutti <mementototem@gmail.com>
	 *
	 */
	private static class PageInfo {
		public int position;
		public ImageView imageView;
	}
}