package net.fingersports.android.failinth;

import java.io.File;
import java.lang.ref.WeakReference;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class ZoomFragment extends SherlockFragment {
	
	private final static boolean LOG = Content.LOG;
	private final static String TAG = "ZoomFragment";
	
	private String mImageUrl;
	private File mCacheDir;
	private Bitmap mBitmap;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		// let's fragment retain (save) its instance
		setRetainInstance(true);
		
		return inflater.inflate(R.layout.fragment_zoom, container, false);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// this will not call when resume (retain instance ignore it)
		Intent parentIntent = getActivity().getIntent();
		mImageUrl = parentIntent.getStringExtra(PagerActivity.EXTRA_IMAGE_URL);
		
		if (LOG) Log.i(TAG, "image url: " + mImageUrl);
		
		// setting up disk cache storage
		mCacheDir = getActivity().getExternalCacheDir();
		
		if (mCacheDir == null) {
			// fall back to internal storage
			mCacheDir = getActivity().getCacheDir();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		ImageView iv = (ImageView) getActivity().findViewById(R.id.zoom_image);
		ProgressBar pb = (ProgressBar) getActivity().findViewById(R.id.zoom_loading);
		
		ImageLoaderTask task = new ImageLoaderTask(iv, pb);
		task.execute(mImageUrl);
		
		Log.d(TAG, "passed");
	}
	
	/**
	 * <p>Helper class for download Image from disk storage.</p>
	 * 
	 * <p>If task can't find that image, try to download it from Internet.</p>
	 * 
	 * @author Apichart Nakarungutti <mementototem@gmail.com>
	 *
	 */
	private class ImageLoaderTask extends AsyncTask<String, Void, Integer> {
		private final static int OK = 1;
		private final static int ERROR = 0;
		
		private final WeakReference<ImageView> imageRef;
		private final WeakReference<ProgressBar> loadingRef;
		
		public ImageLoaderTask(ImageView iv, ProgressBar pb) {
			imageRef = new WeakReference<ImageView>(iv);
			loadingRef = new WeakReference<ProgressBar>(pb);
			
		}
		
		@Override
		protected Integer doInBackground(String... args) {
			
			mBitmap = Content.loadImageFromStorage(mCacheDir, args[0]);
			
			if (mBitmap == null) {
				mBitmap = Content.loadImageFromInternet(args[0]);
			}
			
			if (mBitmap != null) {
				return OK;
			}
			
			return ERROR;
		}
		
		protected void onPostExecute(Integer status) {
			if (status == ERROR || imageRef == null || loadingRef == null) {
				Toast.makeText(getActivity(), "unable to get image", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// get weak reference and set image
			final ImageView iv = imageRef.get();
			if (iv != null) {
				iv.setImageBitmap(mBitmap);
			}
			
			final ProgressBar loading = loadingRef.get();
			if (loading != null) {
				loading.setVisibility(View.GONE);
			}
		}
	}
}
