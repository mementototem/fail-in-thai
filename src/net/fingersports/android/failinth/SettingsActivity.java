package net.fingersports.android.failinth;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import java.util.List;

import com.actionbarsherlock.app.SherlockPreferenceActivity;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends SherlockPreferenceActivity {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */
	private static final boolean ALWAYS_SIMPLE_PREFS = false;
	
	private final static String BLOG_URL = "http://fingersports.net";
	private final static String APP_URL = "http://fingersports.net/app-failinth";
	private final static String FAIL_URL = "http://fail.in.th";
	private final static String DONATE_URL = "http://fingersports.net/donation";
	private final static String MAIL_URL = "mailto:mementototem+failinth@gmail.com?subject=Feedback%20for%20FAIL%20in%20THAI";

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		setupSimplePreferencesScreen();
		
		Preference version = findPreference("pref_about_version");
		version.setOnPreferenceClickListener(versionClickListener);
		
		Preference license = findPreference("pref_about_license");
		license.setOnPreferenceClickListener(licenseClickListener);
		
		Preference feedback = findPreference("pref_about_feedback");
		feedback.setOnPreferenceClickListener(feedbackClickListener);
		
		Preference blog = findPreference("pref_about_blog");
		blog.setOnPreferenceClickListener(blogClickListener);
		
		Preference donate = findPreference("pref_about_donate");
		donate.setOnPreferenceClickListener(donateClickListener);
	}

	/**
	 * Shows the simplified settings UI if the device configuration if the
	 * device configuration dictates that a simplified, single-pane UI should be
	 * shown.
	 */
	@SuppressWarnings("deprecation")
	private void setupSimplePreferencesScreen() {
		if (!isSimplePreferences(this)) {
			return;
		}

		// In the simplified UI, fragments are not used at all and we instead
		// use the older PreferenceActivity APIs.
		
		addPreferencesFromResource(R.xml.pref_about);
		
		// add 'about' preferences, and a corresponding header.
		/*PreferenceCategory fakeHeader = new PreferenceCategory(this);
		fakeHeader.setTitle(R.string.pref_header_donate);
		getPreferenceScreen().addPreference(fakeHeader);
		addPreferencesFromResource(R.xml.pref_donate);*/
		
		// Bind the summaries of Preferences to their values
		// When their values change, their summaries are updated to reflect the new value.
		//bindPreferenceSummaryToValue(findPreference("pref_version"));
	}

	/** {@inheritDoc} */
	@Override
	public boolean onIsMultiPane() {
		return isXLargeTablet(this) && !isSimplePreferences(this);
	}

	/**
	 * Helper method to determine if the device has an extra-large screen. For
	 * example, 10" tablets are extra-large.
	 */
	private static boolean isXLargeTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
	}

	/**
	 * Determines whether the simplified settings UI should be shown. This is
	 * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
	 * doesn't have newer APIs like {@link PreferenceFragment}, or the device
	 * doesn't have an extra-large screen. In these cases, a single-pane
	 * "simplified" settings UI should be shown.
	 */
	private static boolean isSimplePreferences(Context context) {
		return ALWAYS_SIMPLE_PREFS
				|| Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
				|| !isXLargeTablet(context);
	}

	/** {@inheritDoc} */
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onBuildHeaders(List<Header> target) {
		if (!isSimplePreferences(this)) {
			loadHeadersFromResource(R.xml.pref_headers, target);
		}
	}

	/**
	 * A preference value change listener that updates the preference's summary
	 * to reflect its new value.
	 */
	private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object value) {
			String stringValue = value.toString();

			if (preference instanceof ListPreference) {
				// For list preferences, look up the correct display value in
				// the preference's 'entries' list.
				ListPreference listPreference = (ListPreference) preference;
				int index = listPreference.findIndexOfValue(stringValue);

				// Set the summary to reflect the new value.
				preference
						.setSummary(index >= 0 ? listPreference.getEntries()[index]
								: null);
			} else {
				// For all other preferences, set the summary to the value's
				// simple string representation.
				preference.setSummary(stringValue);
			}
			return true;
		}
	};
	
	/**
	 * Open URL (FAIL in THAI section on my blog) if user tab on.
	 */
	private Preference.OnPreferenceClickListener versionClickListener = new Preference.OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			openUrl(APP_URL);
			return false;
		}
	};
	
	/**
	 * Open URL (FAIL web site) if user tab on.
	 */
	private Preference.OnPreferenceClickListener licenseClickListener = new Preference.OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			openUrl(FAIL_URL);
			return false;
		}
	};
	
	/**
	 * Open Mail app and automatic fill e-mail address and header.
	 */
	private Preference.OnPreferenceClickListener feedbackClickListener = new Preference.OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			Intent i = new Intent(Intent.ACTION_SENDTO);
			Uri uri = Uri.parse(MAIL_URL);
			i.setData(uri);
			startActivity(Intent.createChooser(i, getResources().getString(R.string.pref_about_feedback_send)));
			
			return false;
		}
	};
	
	/**
	 * Open URL (my blog) if user tab on.
	 */
	private Preference.OnPreferenceClickListener blogClickListener = new Preference.OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			openUrl(BLOG_URL);
			return false;
		}
	};
	
	/**
	 * Open URL (donate web page) if user tab on.
	 */
	private Preference.OnPreferenceClickListener donateClickListener = new Preference.OnPreferenceClickListener() {
		
		@Override
		public boolean onPreferenceClick(Preference preference) {
			openUrl(DONATE_URL);
			return false;
		}
	};
	
	/**
	 * Helper method for create Intent for open URL
	 * 
	 * @param url Browse URL.
	 */
	private void openUrl(String url) {
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(i);
	}

	/**
	 * Binds a preference's summary to its value. More specifically, when the
	 * preference's value is changed, its summary (line of text below the
	 * preference title) is updated to reflect the value. The summary is also
	 * immediately updated upon calling this method. The exact display format is
	 * dependent on the type of preference.
	 * 
	 * @see #sBindPreferenceSummaryToValueListener
	 */
	@SuppressWarnings("unused")
	private static void bindPreferenceSummaryToValue(Preference preference) {
		// Set the listener to watch for value changes.
		preference
				.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(
				preference,
				PreferenceManager.getDefaultSharedPreferences(
						preference.getContext()).getString(preference.getKey(),
						""));
	}

	/**
	 * This fragment shows general preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class AboutPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_about);
			
			Preference version = findPreference("pref_about_version");
			version.setOnPreferenceClickListener(versionClickListener);
			
			Preference license = findPreference("pref_about_license");
			license.setOnPreferenceClickListener(licenseClickListener);
			
			Preference feedback = findPreference("pref_about_feedback");
			feedback.setOnPreferenceClickListener(feedbackClickListener);
			
			Preference blog = findPreference("pref_about_blog");
			blog.setOnPreferenceClickListener(blogClickListener);
			
			Preference donate = findPreference("pref_about_donate");
			donate.setOnPreferenceClickListener(donateClickListener);

			// Bind the summaries of Preferences to their values
			// When their values change, their summaries are updated to reflect the new value.
			//bindPreferenceSummaryToValue(findPreference("pref_about"));
		}
		
		/**
		 * Open URL (FAIL in THAI section on my blog) if user tab on.
		 */
		private Preference.OnPreferenceClickListener versionClickListener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				openUrl(APP_URL);
				return false;
			}
		};
		
		/**
		 * Open URL (FAIL web site) if user tab on.
		 */
		private Preference.OnPreferenceClickListener licenseClickListener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				openUrl(FAIL_URL);
				return false;
			}
		};
		
		/**
		 * Open Mail app and automatic fill e-mail address and header.
		 */
		private Preference.OnPreferenceClickListener feedbackClickListener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Intent i = new Intent(Intent.ACTION_SENDTO);
				Uri uri = Uri.parse(MAIL_URL);
				i.setData(uri);
				startActivity(Intent.createChooser(i, getResources().getString(R.string.pref_about_feedback_send)));
				
				return false;
			}
		};
		
		/**
		 * Open URL (my blog) if user tab on.
		 */
		private Preference.OnPreferenceClickListener blogClickListener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				openUrl(BLOG_URL);
				return false;
			}
		};
		
		/**
		 * Open URL (donate web page) if user tab on.
		 */
		private Preference.OnPreferenceClickListener donateClickListener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				openUrl(DONATE_URL);
				return false;
			}
		};
		
		/**
		 * Helper method for create Intent for open URL
		 * @param url Browse URL.
		 */
		private void openUrl(String url) {
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(i);
		}
	}
}
