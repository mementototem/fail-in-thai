package net.fingersports.android.failinth;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Custom append comment into ListView instead of use default toString() method called by ListView.
 * 
 * @author Apichart Nakarungutti <mementototem@gmail.com>
 *
 */
public class CommentAdapter extends ArrayAdapter<Comment> {
	private final ArrayList<Comment> mComments;
	private final LayoutInflater mInflater;
	
	public CommentAdapter(Context context, int textViewResourceId, ArrayList<Comment> items) {
		super(context, textViewResourceId, items);
		this.mComments = items;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	/**
	 * <p>This method can do 2 things:</p>
	 * 
	 * <p>First, if this is new view it will inflate view from source and fill comment data
	 * in it.</p>
	 * 
	 * <p>Second, if this is old view (reuse when view out of screen) get there tag 
	 * and fill new comment in to it.</p>
	 * 
	 * @param position Index of comment in ArrayList.
	 * @param convertView View that go out of screen and come back for reuse.
	 * @param parent Parent view of this ListView attached.
	 * 
	 * @return View that ready for display in ListView.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_comment, null);
			
			holder = new ViewHolder();
			holder.user = (TextView) convertView.findViewById(R.id.comment_name);
			holder.time = (TextView) convertView.findViewById(R.id.comment_time);
			holder.msg = (TextView) convertView.findViewById(R.id.comment_message);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Comment comment = mComments.get(position);
		if (comment == null) {
			return convertView;
		}
		
		holder.user.setText(comment.getUser());
		holder.time.setText(comment.getTime());
		holder.msg.setText(comment.getMessage());
		
		return convertView;
	}
	
	/**
	 * <p>I disable View in ListView because comment doesn't has any interaction.</p>
	 * 
	 * @param position Index of comment in ArrayList.
	 */
	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	/**
	 * <p>Helper class for store reference of each View inside ListView's View</p>
	 * 
	 * @author Apichart Nakarungutti <mementototem@gmail.com>
	 */
	private static class ViewHolder {
		public TextView user;
		public TextView time;
		public TextView msg;
	}
}