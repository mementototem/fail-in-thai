package net.fingersports.android.failinth;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class StartActivity extends SherlockFragmentActivity implements
									StartFragment.StartFragmentEventsListener {
	
	@SuppressWarnings("unused")
	private final static String TAG = "StartActivity";
	
	public final static int CODE_WHAT_NEW = 2400; // change this code when update what new
	public final static String KEY_WHAT_NEW = "key_what_new";
	private int whatNew = -1;
	
	StartFragment startFragment = null;
	SharedPreferences sharedPref = null;
	
	int mode = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// load activity layout
		setContentView(R.layout.activity_start);
		
		// get attached fragment
		if (startFragment == null) {
			startFragment = (StartFragment) getSupportFragmentManager().findFragmentById(R.id.start_fragment);
		}
		
		// get shared preferences
		if (sharedPref == null) {
			sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		}
		
		whatNew = sharedPref.getInt(KEY_WHAT_NEW, 0);
		if (whatNew < CODE_WHAT_NEW) {
			displayWhatNew();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.activity_start, menu);
		return true;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onStartFromLatestClick(View v) {
		// set mode to latest and load main activity
		mode = PagerFragment.MODE_LATEST;
		loadMainActivity();
	}

	@Override
	public void onStartFromRandomClick(View v) {
		// set mode to random and load main activity
		mode = PagerFragment.MODE_RANDOM;
		loadMainActivity();
	}

	/**
	 * Open advertise URL in browser.
	 * 
	 * @param v View that call this method.
	 */
	@Override
	public void onAdvClick(View v) {
		String url = startFragment.getAdvUrl();
		
		if (url == null) {
			return;
		}
		
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(i);
	}
	
	/**
	 * Re-check network connection. 
	 * 
	 * @param v View that call this method.
	 */
	@Override
	public void onTryClick(View v) {
		startFragment.checkConnection();
	}
	
	/**
	 * Open Sender Activity for manage and prepare FAIL before send to mail app.
	 * 
	 * @param v View that call this method.
	 */
	@Override
	public void onSendFailClick(View v) {
		// start sender activity
		Intent senderIntent = new Intent(this, SenderActivity.class);
		startActivity(senderIntent);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			onSettingMenuClick(item);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void onSettingMenuClick(MenuItem item) {
		Intent settingsIntent = new Intent(this, SettingsActivity.class);
		startActivity(settingsIntent);
	}
	
	/**
	 * Init main activity (PagerActivity) and switch to it.
	 */
	private void loadMainActivity() {
		// save to shared preferences
		sharedPref.edit().putInt(PagerFragment.KEY_MODE, mode).commit();
		
		Intent mainIntent = new Intent(this, PagerActivity.class);
		startActivity(mainIntent);
	}
	
	/**
	 * Display what's new dialog, if have update about this app
	 * (this might display every time user update the app).
	 */
	private void displayWhatNew() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.start_whatnew_title);
		builder.setMessage(R.string.start_whatnew_message);
		builder.setNegativeButton(R.string.start_whatnew_ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
		
		TextView message = (TextView) dialog.findViewById(android.R.id.message);
		message.setGravity(Gravity.LEFT);
		
		
		// saved code into preference
		sharedPref.edit().putInt(KEY_WHAT_NEW, CODE_WHAT_NEW).commit();
	}
}
